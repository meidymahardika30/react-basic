import React, { Component } from 'react'
import About from '../pages/About'
import Contact from '../pages/Contact'
import Profile from '../pages/Profile'
import Counter from '../pages/Counter'
import FunctionClick from '../pages/FunctionClick'
import ClassClick from '../pages/ClassClick'
import EventBind from '../pages/EventBind'
import ParentComponent from '../pages/ParentComponent'
import UserGreeting from '../pages/UserGreeting'
import NameList from '../pages/NameList'
import Form from '../pages/Form'
import LifecycleA from '../pages/LifecycleA'
import Headers from './Header'
import Siders from './Sider'
import { Layout, Breadcrumb } from 'antd'
import { Route } from 'react-router-dom'

// const { SubMenu } = Menu
const { Content, Footer } = Layout

export default class Layouts extends Component {
    render() {
        return (
            <Layout style={{ height:"100vh" }}>
                <Headers />
                <Layout>
                    <Siders />
                    <Layout style={{ padding: '0 24px 24px' }}>
                    <Breadcrumb style={{ margin: '16px 0' }}>
                        <Breadcrumb.Item>Home</Breadcrumb.Item>
                        <Breadcrumb.Item>List</Breadcrumb.Item>
                        <Breadcrumb.Item>App</Breadcrumb.Item>
                    </Breadcrumb>
                    <Content
                        style={{
                        background: '#fff',
                        padding: 24,
                        margin: 0,
                        minHeight: 280,
                        }}
                    >
                        <Route path="/dashboard/about">
                        <About name="JS" />
                        </Route>

                        <Route path="/dashboard/contact">
                            <Contact primary={true} no="089611575854" />
                        </Route>
                        <Route path="/dashboard/profile" component={ Profile } />
                        <Route path="/dashboard/counter" component={ Counter } />
                        <Route path="/dashboard/function-click">
                            <FunctionClick />
                            <ClassClick />
                            <EventBind />
                            <ParentComponent />
                            <h1>
                                <center>
                                    <UserGreeting />
                                </center>
                            </h1>
                            <NameList />
                        </Route>
                        <Route path="/dashboard/form">
                            <Form />
                            <LifecycleA />
                        </Route>
                    </Content>
                    <Footer style={{ textAlign: 'center' }}>Ant Design ©2019 Created by Muhamad Meidy Mahardika</Footer>
                    </Layout>
                </Layout>
            </Layout>
        )
    }
}
