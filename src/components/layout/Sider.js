import React, { Component } from 'react'
// import About from '../../components/pages/About'
// import Contact from '../../components/pages/Contact'
// import Profile from '../../components/pages/Profile'
// import Counter from '../../components/pages/Counter'
import { Link } from 'react-router-dom'
import { Layout, Menu, Icon } from 'antd'

const { SubMenu } = Menu
const { Sider } = Layout

export default class Siders extends Component {
    render() {
        return (
            <div>
                <Sider width={200} style={{ background: '#fff',height:"80vh" }}>
                    <Menu
                        mode="inline"
                        defaultSelectedKeys={['1']}
                        defaultOpenKeys={['sub1']}
                        style={{ height: '100%', borderRight: 0 }}
                    >
                        <SubMenu
                            key="sub1"
                            title={
                                <span>
                                    <Icon type="user" />
                                subnav 1
                                </span>
                        }
                        >
                        <Menu.Item key="1"><Link to="/dashboard/about">About</Link></Menu.Item>

                        <Menu.Item key="2"><Link to="/dashboard/contact">Contact</Link></Menu.Item>

                        <Menu.Item key="3"><Link to="/dashboard/profile">Profile</Link></Menu.Item>
                        <Menu.Item key="4"><Link to="/dashboard/counter">Counter</Link></Menu.Item>
                        <Menu.Item key="5"><Link to="/dashboard/function-click">Click</Link></Menu.Item>
                        <Menu.Item key="6"><Link to="/dashboard/form">Form</Link></Menu.Item>
                        </SubMenu>
                    </Menu>
                </Sider>
            </div>
        )
    }
}


