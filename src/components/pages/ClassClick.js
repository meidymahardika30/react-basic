import React, { Component } from 'react'

export default class ClassClick extends Component {
    classClick() {
        console.log("Class Click")
    }

    render() {
        return (
            <div>
                <button onClick={this.classClick}>
                    click me
                </button>
            </div>
        )
    }
}
