import React, { Component } from 'react'

export default class EventBind extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             message: 'Hallo!'
        }

        // this.clickHandler = this.clickHandler.bind(this)
    }
    
    // clickHandler() {
    //     this.setState({
    //         message: 'goodbye!'
    //     })
    //     console.log(this)
    // }

    clickHandler = () => {
        this.setState({
            message: 'goodbye!'
        })
    }
    // untuk membawa data

    // click = (event) => {
    //     this.setState({
    //         message: 'goodbye!'
    //     })
    // }
    
    render() {
        const { message } = this.state;

        return (
            <div>
                <div>{message}</div>
                {/* <button onClick={this.clickHandler.bind(this)}>
                    click
                </button> */}

                {/* <button onClick={() => this.clickHandler()}>
                    click
                </button> */}

                <button onClick={this.clickHandler}>
                    click
                </button>

                {/* untuk membawa data */}

                {/* <button onClick={(event) => this.click(event)}>
                    click
                </button> */}
            </div>
        )
    }
}
