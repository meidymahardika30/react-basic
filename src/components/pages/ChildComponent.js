// import React from 'react'

// export default function ChildComponent(props) {
//     return (
//         <div>
//             <button onClick={() => props.greetHandler('Child')}>
//                 Greet Parent
//             </button>
//         </div>
//     )
// }

import React, { Component } from 'react'

export default class ChildComponent extends Component {
    render() {
        return (
            <div>
               <button onClick={() => this.props.greetHandler('Child')}>
                 Greet Parent
               </button> 
            </div>
        )
    }
}
