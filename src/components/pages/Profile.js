import React, { Component } from 'react';

export default class Profile extends Component {
    constructor() {
        super()
        this.state = {
            text: "This is the Profile"
        }
    }

    changeText() {
        this.setState({
            text: "Thank you for subscribing"
        })
    }

    render() {
        return(
            <div>
                <h1>Profile </h1>
                <p>
                    <h3>{this.state.text}</h3>
                </p>

                <button onClick={ () => this.changeText() }>Subscribe</button>

            </div>
        )
    }
}
