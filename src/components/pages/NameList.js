import React from 'react'
// import Person from './Person'

function NameList() {
    
        const names = ['bruce', 'clark', 'diana', 'meidy']
        // const persons = [
        //     {
        //         id: 1,
        //         name: 'Bruce',
        //         age: 17,
        //         skill: 'React'
        //     },
        //     {
        //         id: 2,
        //         name: 'Clark',
        //         age: 18,
        //         skill: 'Vue'
        //     },
        //     {
        //         id: 3,
        //         name: 'Diana',
        //         age: 17,
        //         skill: 'Angular'
        //     }
        // ]

        const nameList = names.map((name, index) => <h2 key={index}>{index+1}. {name}</h2>)
        // const personList = persons.map(person => 
        //     <Person key={person.name} person={person} />
        // )
    return (
        <div>
            {
                nameList
            }
        </div>
    )
}

export default NameList
