import React, { Component } from 'react';
import '../style/Stylesheet.css';

class Contact extends Component {
    render(props) {
        const {no} = this.props
        let title = this.props.primary ? 'title': ''
        return (
            <div>
                <h1 className={`${title} font-xl`}>Contact</h1>
                <p>
                    This is my contact {no}
                </p>
            </div>
        )
    }
}

export default Contact;