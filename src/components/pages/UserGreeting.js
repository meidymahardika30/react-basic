import React, { Component } from 'react'

export default class UserGreeting extends Component {

        constructor(props) {
            super(props)
        
            this.state = {
                 isLoggedIn: false
            }
        }
        

    render() {

        return this.state.isLoggedIn && <div>Welcome Admin</div>

        // cara 3

        // return(
        //     this.state.isLoggedIn ?
        //     <div>Welcome Admin</div> :
        //     <div>Welcome Guest</div>
        // )

        // cara 2
        
        // let message
        // if (this.state.isLoggedIn) {
        //     message =  <div>Welcome Admin</div>
        // } else {
        //     message = <div>Welcome Guest</div>
        // }

        // return <div>{message}</div>

        // cara 1

        // if (this.state.isLoggedIn) {
        //     return (
        //         <div>Hello Admin</div>
        //     )
        // } else {
        //     return (
        //         <div>Hello Guest</div>
        //     )
        // }

        // return (
        //     <div>
        //         <div>Hello Admin</div>
        //         <div>Hello Guest/div>
        //     </div>
        // )
    }
}
