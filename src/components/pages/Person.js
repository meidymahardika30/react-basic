import React from 'react'

function Person({person}) {
    return (
        <div>
            <h2>My Id {person.id}, I am {person.name}, I am {person.age} years old, My skill programming {person.skill}!</h2>
        </div>
    )
}

export default Person
