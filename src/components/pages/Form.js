import React, { Component } from 'react'

export default class Form extends Component {

        constructor(props) {
            super(props)
        
            this.state = {
                 username: '',
                 comments: '',
                 topic: ''
            }
        }
        
        handleUsernameChange = event => {
            this.setState({
                username: event.target.value
            })
        }

        handleCommentsChange = event => {
            this.setState({
                comments: event.target.value
            })
        }

        handleTopicChange = event => {
            this.setState({
                topic: event.target.value
            })
        }

        handleSubmit = event => {
            alert(`${this.state.username} ${this.state.comments} ${this.state.topic}`)
            event.preventDefault()
        }

    render() {
        const { username, comments, topic } = this.state
        return (
            <form onSubmit={this.handleSubmit}>
                <div>
                    <label htmlFor="username">Username:</label>
                    <input 
                        type="text" 
                        id="username" 
                        value={username}
                        onChange={this.handleUsernameChange}
                    />
                </div>
                <div>
                    <label htmlFor="comments">Comments:</label>
                    <textarea 
                        id="comments"
                        value={comments} 
                        onChange={this.handleCommentsChange} 
                    />
                </div>
                <div>
                    <label htmlFor="topic">Topic:</label>
                    <select name="" id="topic" value={topic} onChange={this.handleTopicChange}>
                        <option value=""></option>
                        <option value="react">React</option>
                        <option value="angular">Angular</option>
                        <option value="vue">Vue</option>
                    </select>
                </div>
                <button type="submit">submit</button>
            </form>
        )
    }
}
