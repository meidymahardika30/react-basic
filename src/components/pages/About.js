import React from 'react';
// const Fragment = React.Fragment;

function About(props) {
    const {name} = props

    return (
        <div>
            <h1>About </h1>
            <p>
                This is the About language programming {name}
            </p>

        </div>
    )
}

export default About;