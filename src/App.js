import React, { Component } from 'react'
import Login from './components/pages/Login'
import Layouts from './components/layout/Layout'
import { BrowserRouter as Router, Route } from 'react-router-dom'

// const { SubMenu } = Menu
// const { Content, Footer } = Layout

export default class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>    
          <Route exact path="/">
            <Login />
          </Route>

          <Route path="/dashboard" component={ Layouts } />
        </Router>
      </div>
    )
  }
}